<?php

namespace Drupal\manual_mark_update\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\manual_mark_update\ManualMarkUpdateFieldDefinitionProvider;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Manual mark update module settings form.
 */
class ManualMarkUpdateSettingsForm extends ConfigFormBase {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The entity definition update manager.
   *
   * @var \Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface
   */
  protected EntityDefinitionUpdateManagerInterface $entityDefinitionUpdateManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected EntityFieldManagerInterface $entityFieldManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    ModuleHandlerInterface $module_handler,
    EntityTypeManagerInterface $entity_type_manager,
    EntityDefinitionUpdateManagerInterface $entity_definition_update_manager,
    EntityFieldManagerInterface $entity_field_manager
  ) {
    parent::__construct($config_factory);
    $this->moduleHandler = $module_handler;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityDefinitionUpdateManager = $entity_definition_update_manager;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('module_handler'),
      $container->get('entity_type.manager'),
      $container->get('entity.definition_update_manager'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return 'manual_mark_update.settings';
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'manual_mark_update.settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $config = $this->configFactory->get($this->getEditableConfigNames());

    $form['default_time'] = [
      '#type' => 'radios',
      '#title' => $this->t('Default time'),
      '#description' => $this->t('The default time given to the updated_date field.'),
      '#required' => TRUE,
    ];

    $default_time_options = [
      'none' => $this->t('None - Set only when manually marked'),
      'created' => $this->t('The time the content was created'),
    ];

    // If the publication date module exists add this as an option.
    if ($this->moduleHandler->moduleExists('publication_date')) {
      $default_time_options['publication_date'] = $this->t('Publication date');
    }

    $form['default_time']['#options'] = $default_time_options;
    $form['default_time']['#default_value'] = $config->get('default_time') ?? 'none';

    $form['content_entities'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Enabled Content Entity types'),
      '#description' => $this->t('Only content entity types selected will have this field enabled.'),
    ];

    $options = [];

    // Only load content entity types into the options.
    $all_entity_type_definitions = $this->entityTypeManager->getDefinitions();
    foreach ($all_entity_type_definitions as $entity_type_definition) {
      if ($entity_type_definition->getGroup() == 'content') {
        $options[$entity_type_definition->id()] = $entity_type_definition->getLabel();
      }
    }

    $form['content_entities']['#options'] = $options;
    $form['content_entities']['#default_value'] = $config->get('content_entities') ?? [];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory()->getEditable($this->getEditableConfigNames());

    $content_entities = $form_state->getValue('content_entities');
    $old_content_entities = $config->get('content_entities');

    // Save the configuration.
    $config->set('default_time', $form_state->getValue('default_time'))
      ->set('content_entities', $content_entities)
      ->save();

    $field_definitions = ManualMarkUpdateFieldDefinitionProvider::getAllFieldDefinitions();

    foreach ($content_entities as $content_entity_id => $content_entity_value) {
      if (empty($old_content_entities) || $old_content_entities[$content_entity_id] !== $content_entity_value) {
        foreach ($field_definitions as $id => $definition) {
          if ($content_entity_value) {
            $this->entityDefinitionUpdateManager->installFieldStorageDefinition(
              $id,
              $content_entity_id,
              $content_entity_id,
              $definition
            );
          }
          elseif (!empty($old_content_entities)) {
            $field_storage_definitions = $this->entityFieldManager->getFieldStorageDefinitions($content_entity_id);
            if (isset($field_storage_definitions[$id])) {
              $this->entityDefinitionUpdateManager->uninstallFieldStorageDefinition($field_storage_definitions[$id]);
            }
          }
        }
      }
    }

    parent::submitForm($form, $form_state);
  }

}
