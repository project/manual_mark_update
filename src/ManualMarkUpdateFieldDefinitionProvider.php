<?php

namespace Drupal\manual_mark_update;

use Drupal\Core\Field\BaseFieldDefinition;

final class ManualMarkUpdateFieldDefinitionProvider {

  /**
   * Get the base field definition for the updated date.
   *
   * @return \Drupal\Core\Field\BaseFieldDefinition
   *   Updated date base field definition.
   */
  public static function getUpdatedDateFieldDefinition(): BaseFieldDefinition {
    return BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Updated Date'))
      ->setDescription(t('The date that this content was last marked as updated.'))
      ->setSetting('datetime_type', 'datetime')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'region' => 'hidden',
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'disabled' => TRUE,
        'region' => 'hidden'
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setCardinality(1);
  }

  /**
   * Get the base field definition for the boolean to mark an item as updated.
   *
   * @return \Drupal\Core\Field\BaseFieldDefinition
   *   The update item boolean base field definition.
   */
  public static function getUpdateItemFieldDefinition(): BaseFieldDefinition {
    return BaseFieldDefinition::create('boolean')
      ->setLabel(t('Update Item'))
      ->setDescription(t('Whether to change the "Update date" value of this item.'))
      ->setDisplayOptions('view', [
        'region' => 'hidden',
      ])
      ->setDisplayConfigurable('view', FALSE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', FALSE);
  }

  /**
   * Return all the base field definitions for this module.
   *
   * @return array
   *   Array of all base field definitions, keyed by field name.
   */
  public static function getAllFieldDefinitions(): array {
    return [
      'updated_date' => ManualMarkUpdateFieldDefinitionProvider::getUpdatedDateFieldDefinition(),
      'update_item' => ManualMarkUpdateFieldDefinitionProvider::getUpdateItemFieldDefinition(),
    ];
  }

}
